﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hakkame nimesid küsima:");
            Queue<string> nimed = new Queue<string>();
            Dictionary<string, int> vanused = new Dictionary<string, int>();
            string nimi;
            do
            {
                Console.Write("Anna nimi: ");
                if((nimi = Console.ReadLine()) != "") nimed.Enqueue(nimi);
                
            } while (nimi != "");


            Console.WriteLine("Hakkame vanuseid küsima");
            while(nimed.Count > 0)
            {
                Console.Write($"kui vana on {nimi = nimed.Dequeue()}: ");
                int vanus = int.Parse(Console.ReadLine());
                vanused.Add(nimi, vanus);
            }
            Console.WriteLine("Vaatame, mis sai");
            foreach(var x in vanused)
                Console.WriteLine($"{x.Key} on {x.Value}-aastane");

            Console.WriteLine($"keskmine vanus on {vanused.Values.Average()}");
        }
    }
}
